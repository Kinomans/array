﻿#define _CRT_SECURE_NO_WARNINGS //без этого localtime признана не безопасной, и ничего работать не будет, но
#include <iostream>             //можно было бы и заменить на localtime_s
#include <ctime>  
using namespace std;

int main() {
    setlocale(LC_ALL, "rus");
    int n, m, N, sum = 0;
    cout << "Введите значение N: ";
    cin >> N;
    time_t t;
    time(&t);
    int k = (localtime(&t)->tm_mday) % N;
    cout << "Введите размерность массива: ";
    cin >> n >> m;
    int** a = new int* [n];
    for (int i = 0; i < n; i++)
        a[i] = new int[m];
    cout << "Введите элементы массива:\n";
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
        {
            cin >> a[i][j];
            if (i == k) sum += a[i][j];
        }
    cout << "Сумма элементов строки " << k << " = " << sum;
    for (int i = 0; i < n; i++)
        delete[]a[i];
    delete[] a;
    return 0;
}